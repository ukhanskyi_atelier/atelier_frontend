import React, {} from "react";
import Select from 'react-select'
import ReactPhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import loginImg from "../../images/login_tailor.png";

export class Register extends React.Component {
  constructor(props) {
    super(props);
  }
  state = { phone: "" };

  handleOnChange = value => {
    console.log(value);
    this.setState({ phone: value }, () => {
      console.log(this.state.phone);
    });
  };

  render() {
    const role_options = [
      { value: 'client', label: 'Client' },
      { value: 'admin', label: 'Admin' },
      { value: 'tailor', label: 'Tailor' }
    ]
    const gender_options = [
      { value: 'n/a', label: 'N/A' },
      { value: 'male', label: 'Male' },
      { value: 'female', label: 'Female' }
    ]
    // const phone = phone
    // const [phone, setPhone] = React.useState({
    //   phone: {phone},
    // });

    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="image">
            <img
              src={loginImg}
              alt="Login" />
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="first_name">First name</label>
              <input
                type="text"
                name="first_name"
                placeholder="first_name" />
            </div>
            <div className="form-group">
              <label htmlFor="last_name">Last name</label>
              <input
                type="text"
                name="last_name"
                placeholder="last_name" />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                name="email"
                placeholder="email" />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                placeholder="password" />
            </div>
            <div className="form-group">
              <label htmlFor="address">Address</label>
              <input
                type="text"
                name="address"
                placeholder="address" />
            </div>
            <div className="form-group">
              <label htmlFor="role">Role</label>
              <Select
                options={role_options}
                className="select"
                name="role"
                placeholder="role" />              
            </div>
            <div className="form-group">
              <label htmlFor="gender">Gender</label>
              <Select
                options={gender_options}
                className="select"
                name="gender"
                placeholder="gender"/>              
            </div>
            <div className="form-group">
              <label htmlFor="phone">Phone</label>
              <ReactPhoneInput
                country={'ua'}
                value={this.state.phone}
                onChange={this.handleOnChange}
                inputProps={{
                  name: "phone",
                  country: "ua",
                  required: true,
                  autoFocus: true
                }}
                inputStyle={{
                  paddingLeft: "50px"
                }}
              />
            </div>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btn">
            Register
          </button>
        </div>
      </div>
    );
  }
}
